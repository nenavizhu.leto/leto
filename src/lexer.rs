use std::str::from_utf8;

pub enum TokenType {
    Number,
    Identifier,
    BinaryOperator,
    Equals,
    OpenParen,
    CloseParen,
}

pub struct Token {
    value: String,
    kind: TokenType,
}

impl Token {
    fn new(value: String, kind: TokenType) -> Token {
        Token {
            value: value,
            kind: kind,
        }
    }
}

pub fn tokenize(source_code: String) -> Vec<Token> {
    let mut tokens: Vec<Token> = vec![];
    let mut src = source_code.as_str();

    let mut pointer: usize = 0;
    let mut shift = move || {
        pointer += 1;
    };

    let chars = src.chars().into_iter();

    for ch in chars {
        println!("{:#?}", ch);
        match ch {
            '(' => tokens.push(Token::new(ch.to_string(), TokenType::OpenParen)),
            ')' => tokens.push(Token::new(ch.to_string(), TokenType::CloseParen)),
            '+' | '-' | '*' | '/' | '%' => tokens.push(Token::new(ch.to_string(), TokenType::BinaryOperator)),
            '=' => tokens.push(Token::new(ch.to_string(), TokenType::Equals)),
            '0'..='9' => tokens.push(Token::new(ch.to_string(), TokenType::Number)),
            'A'..='Z' | 'a'..='z' => {
                tokens.push(Token::new(ch.to_string(), TokenType::Identifier)),
            }
        }
    }

    tokens
}
